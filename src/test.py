from IPython import embed
from treelib import Tree
from imp import reload
import preprocess as p
import copy

'''
This file starts an interactive python shell for debugging and testing
'''


def get_tree():
    # Create test tree for planarisation
    tree = Tree()
    tree.create_node("99", "99")
    tree.create_node("21", "21", "99")
    tree.create_node("22", "22", "99")
    tree.create_node("23", "23", "99")
    tree.create_node("31", "31", "21")
    tree.create_node("32", "32", "21")
    tree.create_node("33", "33", "22")
    tree.create_node("1", "1", "31")
    tree.create_node("2", "2", "31")
    tree.create_node("3", "3", "32")
    tree.create_node("4", "4", "33")
    tree.create_node("5", "5", "32")
    tree.create_node("6", "6", "33")
    tree.create_node("7", "7", "32")
    tree.create_node("8", "8", "32")
    tree.create_node("9", "9", "33")
    return tree

t = get_tree()

def import_all():
    xml = p.import_tiger("ressources/tiger_corpus.xml")
    soup = p.make_soup(xml)
    tiger_trees = p.soup_to_graph(soup)
    p.clean_trees(tiger_trees)
    p.adjust_punctuation(tiger_trees)
    return tiger_trees

def reload_src():
    reload(p)

def help():
    print('''
    Load trees with "trees = import_all()"
    If you want to reset the trees often but dont want to wait for loading,
    you can make a deepcopy after import like "ts = copy.deepcopy(trees)".
    After editing the source code of 'preprocess' you can load the changes
    with "reload_src()"
    ''')

# TODO: Start IPython shell with help() an launch
embed()

