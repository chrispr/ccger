import sys
from tqdm import tqdm
from treelib import Tree
from bs4 import BeautifulSoup


def import_tiger(fpath=None):
    """Import the xml file.

    Default: from repo ressource folder
    Optional Argument: from path
    """

    # TODO: Encode corpus as UTF-8
    # if fpath == None:
    #     xml = open("../ressources/tiger_corpus.xml").read()
    # else:
    #     xml = open(fpath).read()

    if fpath is None:
        with open("ressources/tiger_corpus.xml", "r", encoding="latin1") as f:
            xml = f.read()
    else:
        with open(fpath, "r", encoding="latin1") as f:
            xml = f.read()
    return xml


def make_soup(xml):
    """Make soup out of xml for objectification."""
    print("making soup.. (this may take a while)")
    soup = BeautifulSoup(xml, "xml")
    return soup


def get_ids(corpus):
    """Returns a list of all the sentence ids.

    deprecated/dead code: never used
    """

    return [s['id'] for s in corpus.find_all('s')]


def get_roots(corpus):
    """Returns a list of all the root nodes of the graph.

    deprecated/dead code: never used
    """
    return [[g['root'] for g in s.find_all('graph')]
            for s in corpus.find_all('s')]


def get_terminal_node(corpus):
    """Returns a list of all the terminal nodes in the corpus.

    deprecated/dead code: never used
    """

    return [[[[t.attrs for t in ts.find_all('t')]
              for ts in g.find_all('terminals')]
             for g in s.find_all('graph')]
            for s in corpus.find_all('s')]


def get_nonterminal_node(corpus):
    """Returns a list of all the non-terminal nodes in the corpus.

    deprecated/dead code: never used
    """

    return [[[[nt.attrs for nt in nts.find_all('nt')]
              for nts in g.find_all('nonterminals')]
             for g in s.find_all('graph')]
            for s in corpus.find_all('s')]


def get_edges(corpus):
    """Returns a list of all the edges in the corpus.

    deprecated/dead code: never used
    """

    return [[[[e.attrs for e in nts.find_all('edge')]
              for nts in g.find_all('nonterminals')]
             for g in s.find_all('graph')]
            for s in corpus.find_all('s')]


def soup_to_graph(soup):
    """Filter the soup for TigerGraph"""

    corpus = soup.corpus.body
    print("imported corpus")

    # declare tree array in this scope
    trees = []

    print("making trees:")

    # get all sentences of the corpus
    for s in tqdm(corpus.find_all('s')):

        # select the graph
        g = s.graph

        # create empty tree
        tree = Tree()

        # insert root node in tree
        tree.create_node(g['root'], g['root'], data=g.attrs)

        # insert terminal nodes in tree
        for t in g.terminals.find_all('t'):
            if tree.contains(t['id']):
                tree.get_node(t['id']).data = t.attrs
            else:
                tree.create_node(t['id'], t['id'],
                                 parent=g['root'],
                                 data=t.attrs)

        # declare secondary edges in this scope
        sedges = []

        # insert nonterminal nodes in tree
        for nt in g.nonterminals.find_all('nt'):
            if tree.contains(nt['id']):
                tree.get_node(nt['id']).data['cat'] = nt['cat']
            else:
                tree.create_node(nt['id'], nt['id'],
                                 parent=g['root'],
                                 data={'cat': nt['cat']})

            # connect nodes
            for edge in nt.find_all('edge'):
                # 'try' is needed, because in 63 graphs there are edges
                # referencing not-yet initialized nodes.
                # (See ressource/bad_nodes.txt)
                # For now we are ignoring this problem because of the
                # insignificance (~0.12% of all nodes)
                # DONE #A: Clean list of uncomplete graphs:
                # Use functions to break outer loop and prevent
                # 'trees.append(tree)'
                # Result at ccger/results/bad_trees.txt
                # TODO #B: Cover bad-ordered graphs too
                try:
                    tree.move_node(edge['idref'], nt['id'])
            # introduce the 'elabel' data attribute for edge label preservation
                    tree.get_node(edge['idref']).data['elabel'] = edge['label']
                except:
                    break

            # save secondary edges in the graph root
            # (can't insert into tree, because it would not be a tree anymore)
            sedge = nt.secedge
            if sedge is not None:
                sedges.append((nt['id'], sedge['idref'], sedge['label']))

        if len(sedges) > 0:
            tree.get_node(tree.root).data['sedges'] = sedges

        trees.append(tree)

    return trees


def strip_id(str_id, x):
    """Helper function to clean the ids of unwanted artefacts."""

    try:
        new_id = str_id.split('_')[x]
        return new_id
    except:
        print("You made a mistake. The id doesn't have " +
              x+1 +
              " partition(s)")


def clean_trees(trees):
    """Function to clean the list of trees of the 63 badly ordered graphs.

    [This is just a hack to meet the deadline! See TODO in line 142]

    deprecated because not needed anymore
    """

    bad_trees = open("ressources/bad_nodes.txt").read()
    bad_id = set()

    for bt in bad_trees.splitlines():
        bad_id.add(strip_id(bt, 0))

    for t in trees:
        if strip_id(t.root, 0) in bad_id:
            trees.remove(t)

    return trees


def adjust_punctuation(trees):
    """In the Tiger corpus punctuations are attached to the VROOT per default.
    This function connects it to the appropriate node.

    TODO #A: some trees have no VROOT and the punctuation is attached to the
    root-node. This function doesnt cover those cases yet.
    """

    print("adjust punctuation:")

    for t in tqdm(trees):

        # get sentence id
        sid = strip_id(t.root, 0)

        # Because TIGER corpus marks parentheses and quotes only with
        # pos="$(" and not the closing part with pos"$)" respectively
        # we have to find out for ourselves to know where to put the
        # node. We do this by carrying a bool. We may not cover all
        # cases like quotes or parentheses within quotes. But we will
        # ignore potential errors for now since they are most likely
        # very rare.
        # TODO #A: quantify sentences with >= 3 "$("
        # TODO #B: cover those sentences too
        opening = True

        for node in t.children(t.root):

            # filter for punctuation nodes
            try:
                if "$" in node.data['pos']:

                    # get node id
                    nid = strip_id(node.identifier, 1)

                    # filter for opening punctuation
                    if "(" in node.data['pos'] and opening is True:

                        # npid = new parent id
                        npid = t.parent(sid + "_" + str(int(nid)+1)).identifier
                        opening = False
                        if node.data['word'] == "-":
                            npid = t.parent(sid +
                                            "_" +
                                            str(int(nid)-1)).identifier
                            opening = True
                        t.move_node(node.identifier, npid)

                    elif "(" in node.data['pos'] and opening is False:
                        npid = t.parent(sid + "_" + str(int(nid)-1)).identifier
                        opening = True

                    else:
                        npid = t.parent(sid + "_" + str(int(nid)-1)).identifier
                        t.move_node(node.identifier, npid)

            # if not an terminal node it passes
            except:
                continue


def planarize(trees):
    """
    reform the tree to a planar form, so there are no crossing edges
    as the nodes are sorted by end incices,
    if the current child node starts earlier than a former child node, there
    have to be crossing edges. In that case all child nodes of the current
    child, which are positioned before a former node are lifted to be children
    of the current parent node.

    :param trees: list of all TIGER trees
    :return: list of all planarized TIGER trees
    """

    # Define Helper Functions
    def check_continuity(tree):
        # remember the tree id
        tree_id = strip_id(tree.root, 0)
        # define 'super tuple' we need later
        super_tuple = []
        # initialize leave map
        leaves = {}
        for path in tree.paths_to_leaves():
            leaf = path[-1]
            # ignore the leaves
            for j in path[:-1]:
                if j not in leaves.keys():
                    # make sure this is no unwanted leaf
                    if tree.get_node(j).is_leaf():
                        continue
                    leaves[j] = set()
                leaves.get(j).add(leaf)

        # put leaves in list and sort them
        for node, leafs in leaves.items():
            # strip off the tree id
            l = [int(strip_id(x, 1)) for x in leafs]
            l.sort()
            p = (node, l)
            # print(p) # debug
            super_tuple.append(p)

            def sort_helper(tup):
                return strip_id(tup[0], 1)
            super_tuple.sort(key=sort_helper)

            def sort_helper2(tup):
                return len(tup[1])
            super_tuple.sort(key=sort_helper2)

        for j in range(len(super_tuple)):
            node = super_tuple[j]
            parent = node[0]
            parentn = tree.get_node(parent)
            leafs = node[1]
            n = leafs[0]
            for i in range(len(leafs)):
                if leafs[i] == n:
                    continue
                elif leafs[i] == n+1:
                    n = n+1
                    if i+1 == len(leafs):
                        if j+1 == len(super_tuple):
                            return True
                        continue

                else:
                    bastards = leafs[:i]
                    grandparent = parentn.bpointer
                    while len(bastards) > 0:
                        bob = bastards.pop()
                        nid = tree_id + "_" + str(bob)
                        if nid in parentn.fpointer:
                            tree.move_node(nid, grandparent)

                        else:
                            ninst = tree.get_node(nid)
                            leaves = []
                            while ninst.identifier not in parentn.fpointer:
                                if ninst.identifier != tree.root:
                                    ninst = tree.get_node(ninst.bpointer)
                                    nid = ninst.identifier
                                else:
                                    print("something went wrong..")
                                    break
                            leaves = tree.leaves(nid)
                            bastards.append(bob)
                            for leaf in leaves:
                                # Sibling must be leaf
                                if leaf.is_leaf() is False:
                                    lid = leaf.identifier
                                    for leef in tree.leaves(lid):
                                        b = int(strip_id(leef.identifier, 1))
                                        bastards.remove(b)
                                        return False
                                else:
                                    b = int(strip_id(leaf.identifier, 1))
                                    bastards.remove(b)
                            tree.move_node(nid, grandparent)
                    return False

    def recursion(tree):
        boo = check_continuity(tree)
        if boo is True:
            return tree
        else:
            recursion(tree)

    print("planarize trees:")

    for t in tqdm(trees):
        root = t.get_node(t.root)

        # check if tree is discontinuous:
        if 'discontinuous' in root.data.keys() \
           and root.identifier != "s24_VROOT":
            recursion(t)


def binarize(trees):
    # TODO
    btrees = trees
    return btrees


def categorize(trees):
    # TODO
    return trees


def print_trees(trees):
    """Function to print trees"""

    for tree in trees:
        tree.show(line_type="ascii-em")
        print("-------------------------------------\n")


def run(path=None):
    xml = import_tiger(path)
    soup = make_soup(xml)
    tiger_trees = soup_to_graph(soup)
    clean_trees(tiger_trees)
    adjust_punctuation(tiger_trees)
    planarize(tiger_trees)
    return tiger_trees

if __name__ == "__main__":
    if len(sys.argv) == 2:
        path = sys.argv[1]
        xml = import_tiger(path)
        soup = make_soup(xml)
        tiger_trees = soup_to_graph(soup)
        clean_trees(tiger_trees)
        adjust_punctuation(tiger_trees)
        planarize(tiger_trees)
        print_trees(tiger_trees)
    else:
        try:
            tiger_trees = import_tiger("ressources/tiger_corpus.xml")
        except:
            print("Usage:\
            python import_tiger.py [optional: /path/to/tiger_corpus.xml]")
