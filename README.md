# ccger - A CCGBank for German

## Usage

Clone the repo:

    $ git clone git@gitlab.com:CCG_Treebank_Induction/ccger.git
    $ cd ccger


Install and activate virtual environment:

    ccger $ virtualenv -p python3 venv
    ccger $ source venv/bin/activate


Install dependencies:

    (venv) $ pip3 install beautifulsoup4 lxml treelib tqdm ipython

Create trees:

    (venv) $ python3 src/preprocess.py ressources/tiger_corpus.xml

Use ipython shell to work interactively with the data:

    (venv) $ python3 src/test.py
    In [1]: help()

Run tests: (not implemented yet)

    (venv) $ python3 tests/runtests.py

